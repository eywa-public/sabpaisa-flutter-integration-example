import UIKit
import Flutter
import SabPaisa_IOS_Sdk

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    
    var initUrl="https://sdkstaging.sabpaisa.in/SabPaisa/sabPaisaInit?v=1"
    var baseUrl="https://sdkstaging.sabpaisa.in"
    var transactionEnqUrl="https://stage-txnenquiry.sabpaisa.in"
    
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
      
      let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
          let batteryChannel = FlutterMethodChannel(name: "samples.flutter.dev/battery",
                                                    binaryMessenger: controller.binaryMessenger)
         
                    batteryChannel.setMethodCallHandler({
                        [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
                    
                    // Note: this method is invoked on the UI thread.
                    guard call.method == "callSabPaisaSdk" else {
                     result(FlutterMethodNotImplemented)
                     return
                    }
                        if let dict = call.arguments as? [String] {
                            print("hello swift")
                            self?.openSdk(firstName: "sabpaisa",lastName: "lastname",emailId: dict[2],mobileNumber: "3443232322",amount: "2000",result1: result)
                        }
                       
                })
      
      
    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
    private func openSdk(firstName:String, lastName:String, emailId:String, mobileNumber:String,
                         amount:String,result1: @escaping FlutterResult) {
        
        
        let bundle = Bundle(identifier: "com.eywa.ios.SabPaisa-IOS-Sdk")
        let storyboard = UIStoryboard(name: "Storyboard", bundle: bundle)
        
        
        let secKey="kaY9AIhuJZNvKGp2"
        let secInivialVector="YN2v8qQcU3rGfA1y"
        let transUserName="rajiv.moti_336"
        let transUserPassword="RIADA_SP336"
        let clientCode="TM001"
        
        
        let vc = storyboard.instantiateViewController(withIdentifier: "InitialLoadViewController_Identifier") as! InitialLoadViewController
        vc.sdkInitModel=SdkInitModel(firstName: firstName, lastName: lastName, secKey: secKey, secInivialVector: secInivialVector, transUserName: transUserName, transUserPassword: transUserPassword, clientCode: clientCode, amount: Float(amount)!,emailAddress: emailId,mobileNumber: mobileNumber,isProd: false,baseUrl: baseUrl, initiUrl: initUrl,transactionEnquiryUrl: transactionEnqUrl)
        
        
        vc.callback =  { (response:TransactionResponse)  in
            print("---------------Final Response To USER------------")
            vc.dismiss(animated: true)
            
            result1([response.status,response.clientTxnId])
        }
        if let topController = UIApplication.shared.keyWindow?.rootViewController {
            topController.present(vc, animated: true, completion: nil)
        }
        
    }
}



